console.log("Hello World!");


// It will comment single line of codes

/*
	This will comment multi-lines of codes


	*/

// [Section] Syntax, Statements
	// Statements in programming are instructions that we tell the computer to perform

	// JS Statements usually end with semicolon (;)

	// Semicolons are not required in JS but we will use it to help us train to locate where statements ends


	// A Syntax in programming, it is the set of rules that we describes statements must be constructed



let myVariable = "Ada Lovelace";
let variable;
console.log(variable);
const constVariable = "John Doe";
console.log(myVariable);



let productName = "desktop computer"

// Declaration
let desktopName;



// Initialization of the value of variable desktopName
desktopName = "Dell"

console.log(desktopName);

productName = "Personal Computer";

console.log(productName);


const name = "Chris";
console.log(name);
/*name = "Topher";
console.log(name);*/


/////////////////////////////

//let/const local/global scope

/*

	Scopre essentially means where these variables are available to use.

	let/const are block scope

	A block is a chunk of code bounded by  {}.


*/


/*let outerVariable = "Hello";


{
	let innerVariable = "Hello Again";
	console.log(innerVariable);
}

console.log(outerVariable);
*/


/*const outerVariable = "Hello"
{
	const innerVariable = "Hello Again";
}

console.log(outerVariable);
console.log(innerVariable);*/

var outerVariable = "Hello"
{
	var innerVariable = "Hello Again";
}

console.log(outerVariable);
console.log(innerVariable);


//Multiple variable declarations

let productCode = "DC017", productBrand = "Dell";
console.log(productCode);
console.log(productBrand);

//[SECTION] Data Types
// Strings
/*
	Strings are a series of characters that create a word, a phrase or a sentence or anything related to creating a text
	Strings in JavaScript can be written using either a single quote ('') or double quote ("")


*/

let country = "Philippines";
let province = 'Metro Manila';


console.log(country);
console.log(province);


let fullAddress = province + ', ' + country;

console.log(fullAddress);

let greeting = 'I live in the ' + country;

console.log(greeting);

//Declaring a string using an escape character
let message = 'John\'s employees went home early';
console.log(message);

message = "John's employees went home early";
console.log(message);

//"\n" - refers to creating a new line in between text

let mailAddress = "Metro Manila \n\n Philippines";
console.log(mailAddress);


//Numbers
//Integer/Whole Number
let headCount = 27;
console.log(headCount);

//Decimal Number
let grade = 98.7;
console.log(grade);

//Exponential Notation
let planetDistance = 2e10;
console.log(planetDistance);


//Combining text and strings
console.log("John's grade last quarter is " + grade);


//Boolean
//Boolean values are normally used to store values relating to the state of certain things
// true of false
let isMarreid = false;
let inGoodConduct = true;

console.log(isMarreid);
console.log(inGoodConduct);



//Arrays
//Arrays are a special kind of data type that's use to store multiple values

/*
	Syntax:
		let/const arraName = [elementA,elementB, ...]

*/

//similar data types
let grades = [98,92.1,90.1,94.7];
console.log(grades);


//different data type
let details = ["John",32,true];
console.log(details);

//Objects
//Objects are another special kind of data type that's used to mimic real world objects/items


/*
	Syntax:
		let/const objectName = {
			propertyA: value,
			propertyB: value
		}

*/


let person = {
	firstName: "John",
	lastName: "Smith",
	age: 32,
	isMarried: false,
	contact: ["+639123456789", "81234567"],
	address: {
		houseNumber: "345",
		city: "Manila"
	}
}

console.log(person)


let myGrades = {
	firstGrading: 98,
	secondGrading: 92.1,
	thirdGrading: 90.1,
	fourthGrading: 94.7
}


/*
	Constant Objects and Arrays



*/

const anime = ["one piece","one punch man","your lie in april"];
console.log(anime);
anime[0] = 'kimetsu no yaiba';
console.log(anime);



//Null
//It is used to intentionally express the absence of a value in a variable declaration/initialization

let spouse = null;
console.log(null);


//Undefined
//Represent the state of a variable that has been declared but without an assigned value

let inARelationship;
console.log(inARelationship);


